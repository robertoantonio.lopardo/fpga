#include <stdio.h>
#include <stdlib.h>
#include "hls_macc.h"

// Internal function prototypes
int ref_macc(basetype x, basetype *y);

// Program entry for C test bench which is used to validate HLS DUT
//  functionality and automatically reused by Vivado HLS cosim_design
//  for RTL verification
int main(void)
{
   const basetype num_tests = 32;
   basetype x, y, yref;
   int i;
   int err_cnt = 0;

   for (i = 0; i < num_tests; i++) {
      // Generate random test data
      x = (rand() - RAND_MAX / 2) >> (8*sizeof(int) - 12);
      hls_macc(x, &y);
      ref_macc(x, &yref);

      // Check HW result vs reference result
      if (y != yref) {
         printf("!!! ERROR mismatch on test #%d - ", i);
         printf("HW returned: %d; ", (int) y);
         printf("Expected: %d !!!\n", (int) yref);
         err_cnt++;
      }
   }
   if (err_cnt)
      printf("\n!!! %d TESTS FAILED !!!\n\n", err_cnt);
   else
      printf("\n*** %d Tests Passed ***\n\n", num_tests);
   // Always return 0 on success
   return err_cnt;
}

// Definition of reference software model of DUT
int ref_macc(basetype x, basetype *y)
{
    basetype s = 0;
    for (int i = 1; i <= 100; i++)
        s += i * (x - (5 * i));
    *y = s;
}

