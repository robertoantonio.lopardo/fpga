#ifndef HLS_MACC_H_
#define HLS_MACC_H_

#include <stdbool.h>

typedef double basetype;

void hls_macc(basetype x, basetype *y);

#endif //#ifndef HLS_MACC_H_

