#include "hls_macc.h"

void hls_macc(basetype x, basetype *y)
{
#pragma HLS INTERFACE s_axilite port=return bundle=HLS_MACC_PERIPH_BUS
#pragma HLS INTERFACE s_axilite port=x bundle=HLS_MACC_PERIPH_BUS
#pragma HLS INTERFACE s_axilite port=y bundle=HLS_MACC_PERIPH_BUS

    basetype s = 0;
    for (int i = 1; i <= 100; i++)
        s += i * (x - (5 * i));
    *y = s;
}
