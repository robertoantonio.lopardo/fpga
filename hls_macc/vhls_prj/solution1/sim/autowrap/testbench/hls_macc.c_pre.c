# 1 "/home/roberto/HPC/heterogeneous/FPGA/Project/original_code/hls_macc/hls_macc.c"
# 1 "/home/roberto/HPC/heterogeneous/FPGA/Project/original_code/hls_macc/hls_macc.c" 1
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 149 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/home/roberto/HPC/heterogeneous/FPGA/Project/original_code/hls_macc/hls_macc.c" 2
# 1 "/home/roberto/HPC/heterogeneous/FPGA/Project/original_code/hls_macc/hls_macc.h" 1



# 1 "/home/roberto/Vivado/Xilinx/Vivado/2019.1/lnx64/tools/clang/bin/../lib/clang/3.1/include/stdbool.h" 1 3
# 5 "/home/roberto/HPC/heterogeneous/FPGA/Project/original_code/hls_macc/hls_macc.h" 2

typedef double basetype;

void hls_macc(basetype x, basetype *y);
# 2 "/home/roberto/HPC/heterogeneous/FPGA/Project/original_code/hls_macc/hls_macc.c" 2

void hls_macc(basetype x, basetype *y)
{
#pragma HLS INTERFACE s_axilite port=return bundle=HLS_MACC_PERIPH_BUS
#pragma HLS INTERFACE s_axilite port=x bundle=HLS_MACC_PERIPH_BUS
#pragma HLS INTERFACE s_axilite port=y bundle=HLS_MACC_PERIPH_BUS

 basetype s = 0;
    for (int i = 1; i <= 100; i++)
        s += i * (x - (5 * i));
    *y = s;
}
