# This script segment is generated automatically by AutoPilot

set axilite_register_dict [dict create]
set port_HLS_MACC_PERIPH_BUS {
ap_start { }
ap_done { }
ap_ready { }
ap_idle { }
x { 
	dir I
	width 64
	depth 1
	mode ap_none
	offset 16
	offset_end 27
}
y { 
	dir O
	width 64
	depth 1
	mode ap_vld
	offset 28
	offset_end 39
}
}
dict set axilite_register_dict HLS_MACC_PERIPH_BUS $port_HLS_MACC_PERIPH_BUS


