create_project prj -part xc7z020-clg484-1 -force
set_property target_language verilog [current_project]
set vivado_ver [version -short]
set COE_DIR "../../syn/verilog"
source "/home/roberto/HPC/heterogeneous/FPGA/Project/original_code/hls_macc/vhls_prj/solution1/syn/verilog/hls_macc_ap_dmul_16_max_dsp_64_ip.tcl"
if {[regexp -nocase {2015\.3.*} $vivado_ver match] || [regexp -nocase {2016\.1.*} $vivado_ver match]} {
    extract_files -base_dir "./prjsrcs/sources_1/ip" [get_files -all -of [get_ips hls_macc_ap_dmul_16_max_dsp_64]]
}
source "/home/roberto/HPC/heterogeneous/FPGA/Project/original_code/hls_macc/vhls_prj/solution1/syn/verilog/hls_macc_ap_sitodp_6_no_dsp_32_ip.tcl"
if {[regexp -nocase {2015\.3.*} $vivado_ver match] || [regexp -nocase {2016\.1.*} $vivado_ver match]} {
    extract_files -base_dir "./prjsrcs/sources_1/ip" [get_files -all -of [get_ips hls_macc_ap_sitodp_6_no_dsp_32]]
}
source "/home/roberto/HPC/heterogeneous/FPGA/Project/original_code/hls_macc/vhls_prj/solution1/syn/verilog/hls_macc_ap_dadddsub_14_full_dsp_64_ip.tcl"
if {[regexp -nocase {2015\.3.*} $vivado_ver match] || [regexp -nocase {2016\.1.*} $vivado_ver match]} {
    extract_files -base_dir "./prjsrcs/sources_1/ip" [get_files -all -of [get_ips hls_macc_ap_dadddsub_14_full_dsp_64]]
}
