// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
// HLS_MACC_PERIPH_BUS
// 0x00 : Control signals
//        bit 0  - ap_start (Read/Write/COH)
//        bit 1  - ap_done (Read/COR)
//        bit 2  - ap_idle (Read)
//        bit 3  - ap_ready (Read)
//        bit 7  - auto_restart (Read/Write)
//        others - reserved
// 0x04 : Global Interrupt Enable Register
//        bit 0  - Global Interrupt Enable (Read/Write)
//        others - reserved
// 0x08 : IP Interrupt Enable Register (Read/Write)
//        bit 0  - Channel 0 (ap_done)
//        bit 1  - Channel 1 (ap_ready)
//        others - reserved
// 0x0c : IP Interrupt Status Register (Read/TOW)
//        bit 0  - Channel 0 (ap_done)
//        bit 1  - Channel 1 (ap_ready)
//        others - reserved
// 0x10 : Data signal of x
//        bit 31~0 - x[31:0] (Read/Write)
// 0x14 : Data signal of x
//        bit 31~0 - x[63:32] (Read/Write)
// 0x18 : reserved
// 0x1c : Data signal of y
//        bit 31~0 - y[31:0] (Read)
// 0x20 : Data signal of y
//        bit 31~0 - y[63:32] (Read)
// 0x24 : Control signal of y
//        bit 0  - y_ap_vld (Read/COR)
//        others - reserved
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

#define XHLS_MACC_HLS_MACC_PERIPH_BUS_ADDR_AP_CTRL 0x00
#define XHLS_MACC_HLS_MACC_PERIPH_BUS_ADDR_GIE     0x04
#define XHLS_MACC_HLS_MACC_PERIPH_BUS_ADDR_IER     0x08
#define XHLS_MACC_HLS_MACC_PERIPH_BUS_ADDR_ISR     0x0c
#define XHLS_MACC_HLS_MACC_PERIPH_BUS_ADDR_X_DATA  0x10
#define XHLS_MACC_HLS_MACC_PERIPH_BUS_BITS_X_DATA  64
#define XHLS_MACC_HLS_MACC_PERIPH_BUS_ADDR_Y_DATA  0x1c
#define XHLS_MACC_HLS_MACC_PERIPH_BUS_BITS_Y_DATA  64
#define XHLS_MACC_HLS_MACC_PERIPH_BUS_ADDR_Y_CTRL  0x24

