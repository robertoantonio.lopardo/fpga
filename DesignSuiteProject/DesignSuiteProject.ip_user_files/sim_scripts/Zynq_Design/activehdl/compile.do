vlib work
vlib activehdl

vlib activehdl/xilinx_vip
vlib activehdl/xil_defaultlib
vlib activehdl/xpm
vlib activehdl/axi_infrastructure_v1_1_0
vlib activehdl/axi_vip_v1_1_5
vlib activehdl/processing_system7_vip_v1_0_7
vlib activehdl/xbip_utils_v3_0_10
vlib activehdl/axi_utils_v2_0_6
vlib activehdl/xbip_pipe_v3_0_6
vlib activehdl/xbip_dsp48_wrapper_v3_0_4
vlib activehdl/xbip_dsp48_addsub_v3_0_6
vlib activehdl/xbip_dsp48_multadd_v3_0_6
vlib activehdl/xbip_bram18k_v3_0_6
vlib activehdl/mult_gen_v12_0_15
vlib activehdl/floating_point_v7_1_8
vlib activehdl/generic_baseblocks_v2_1_0
vlib activehdl/fifo_generator_v13_2_4
vlib activehdl/axi_data_fifo_v2_1_18
vlib activehdl/axi_register_slice_v2_1_19
vlib activehdl/axi_protocol_converter_v2_1_19
vlib activehdl/lib_cdc_v1_0_2
vlib activehdl/proc_sys_reset_v5_0_13

vmap xilinx_vip activehdl/xilinx_vip
vmap xil_defaultlib activehdl/xil_defaultlib
vmap xpm activehdl/xpm
vmap axi_infrastructure_v1_1_0 activehdl/axi_infrastructure_v1_1_0
vmap axi_vip_v1_1_5 activehdl/axi_vip_v1_1_5
vmap processing_system7_vip_v1_0_7 activehdl/processing_system7_vip_v1_0_7
vmap xbip_utils_v3_0_10 activehdl/xbip_utils_v3_0_10
vmap axi_utils_v2_0_6 activehdl/axi_utils_v2_0_6
vmap xbip_pipe_v3_0_6 activehdl/xbip_pipe_v3_0_6
vmap xbip_dsp48_wrapper_v3_0_4 activehdl/xbip_dsp48_wrapper_v3_0_4
vmap xbip_dsp48_addsub_v3_0_6 activehdl/xbip_dsp48_addsub_v3_0_6
vmap xbip_dsp48_multadd_v3_0_6 activehdl/xbip_dsp48_multadd_v3_0_6
vmap xbip_bram18k_v3_0_6 activehdl/xbip_bram18k_v3_0_6
vmap mult_gen_v12_0_15 activehdl/mult_gen_v12_0_15
vmap floating_point_v7_1_8 activehdl/floating_point_v7_1_8
vmap generic_baseblocks_v2_1_0 activehdl/generic_baseblocks_v2_1_0
vmap fifo_generator_v13_2_4 activehdl/fifo_generator_v13_2_4
vmap axi_data_fifo_v2_1_18 activehdl/axi_data_fifo_v2_1_18
vmap axi_register_slice_v2_1_19 activehdl/axi_register_slice_v2_1_19
vmap axi_protocol_converter_v2_1_19 activehdl/axi_protocol_converter_v2_1_19
vmap lib_cdc_v1_0_2 activehdl/lib_cdc_v1_0_2
vmap proc_sys_reset_v5_0_13 activehdl/proc_sys_reset_v5_0_13

vlog -work xilinx_vip  -sv2k12 "+incdir+/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
"/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
"/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
"/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
"/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
"/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
"/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_if.sv" \
"/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/clk_vip_if.sv" \
"/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/rst_vip_if.sv" \

vlog -work xil_defaultlib  -sv2k12 "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/ec67/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/8c62/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ip/Zynq_Design_processing_system7_0_0" "+incdir+/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work axi_infrastructure_v1_1_0  -v2k5 "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/ec67/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/8c62/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ip/Zynq_Design_processing_system7_0_0" "+incdir+/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/ec67/hdl/axi_infrastructure_v1_1_vl_rfs.v" \

vlog -work axi_vip_v1_1_5  -sv2k12 "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/ec67/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/8c62/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ip/Zynq_Design_processing_system7_0_0" "+incdir+/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/d4a8/hdl/axi_vip_v1_1_vl_rfs.sv" \

vlog -work processing_system7_vip_v1_0_7  -sv2k12 "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/ec67/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/8c62/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ip/Zynq_Design_processing_system7_0_0" "+incdir+/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/8c62/hdl/processing_system7_vip_v1_0_vl_rfs.sv" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/ec67/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/8c62/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ip/Zynq_Design_processing_system7_0_0" "+incdir+/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/Zynq_Design/ip/Zynq_Design_processing_system7_0_0/sim/Zynq_Design_processing_system7_0_0.v" \

vcom -work xbip_utils_v3_0_10 -93 \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/1123/hdl/xbip_utils_v3_0_vh_rfs.vhd" \

vcom -work axi_utils_v2_0_6 -93 \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/1971/hdl/axi_utils_v2_0_vh_rfs.vhd" \

vcom -work xbip_pipe_v3_0_6 -93 \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/7468/hdl/xbip_pipe_v3_0_vh_rfs.vhd" \

vcom -work xbip_dsp48_wrapper_v3_0_4 -93 \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/cdbf/hdl/xbip_dsp48_wrapper_v3_0_vh_rfs.vhd" \

vcom -work xbip_dsp48_addsub_v3_0_6 -93 \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/910d/hdl/xbip_dsp48_addsub_v3_0_vh_rfs.vhd" \

vcom -work xbip_dsp48_multadd_v3_0_6 -93 \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/b0ac/hdl/xbip_dsp48_multadd_v3_0_vh_rfs.vhd" \

vcom -work xbip_bram18k_v3_0_6 -93 \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/d367/hdl/xbip_bram18k_v3_0_vh_rfs.vhd" \

vcom -work mult_gen_v12_0_15 -93 \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/d4d2/hdl/mult_gen_v12_0_vh_rfs.vhd" \

vcom -work floating_point_v7_1_8 -93 \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/83a3/hdl/floating_point_v7_1_vh_rfs.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/ec67/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/8c62/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ip/Zynq_Design_processing_system7_0_0" "+incdir+/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/13a6/hdl/verilog/hls_macc_dadddsub_64ns_64ns_64_16_full_dsp_1.v" \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/13a6/hdl/verilog/hls_macc_dmul_64ns_64ns_64_18_max_dsp_1.v" \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/13a6/hdl/verilog/hls_macc_HLS_MACC_PERIPH_BUS_s_axi.v" \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/13a6/hdl/verilog/hls_macc_sitodp_32ns_64_8_1.v" \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/13a6/hdl/verilog/hls_macc.v" \

vcom -work xil_defaultlib -93 \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/13a6/hdl/ip/hls_macc_ap_dmul_16_max_dsp_64.vhd" \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/13a6/hdl/ip/hls_macc_ap_dadddsub_14_full_dsp_64.vhd" \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/13a6/hdl/ip/hls_macc_ap_sitodp_6_no_dsp_32.vhd" \
"../../../bd/Zynq_Design/ip/Zynq_Design_hls_macc_0_0/sim/Zynq_Design_hls_macc_0_0.vhd" \

vlog -work generic_baseblocks_v2_1_0  -v2k5 "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/ec67/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/8c62/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ip/Zynq_Design_processing_system7_0_0" "+incdir+/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/b752/hdl/generic_baseblocks_v2_1_vl_rfs.v" \

vlog -work fifo_generator_v13_2_4  -v2k5 "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/ec67/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/8c62/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ip/Zynq_Design_processing_system7_0_0" "+incdir+/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/1f5a/simulation/fifo_generator_vlog_beh.v" \

vcom -work fifo_generator_v13_2_4 -93 \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/1f5a/hdl/fifo_generator_v13_2_rfs.vhd" \

vlog -work fifo_generator_v13_2_4  -v2k5 "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/ec67/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/8c62/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ip/Zynq_Design_processing_system7_0_0" "+incdir+/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/1f5a/hdl/fifo_generator_v13_2_rfs.v" \

vlog -work axi_data_fifo_v2_1_18  -v2k5 "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/ec67/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/8c62/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ip/Zynq_Design_processing_system7_0_0" "+incdir+/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/5b9c/hdl/axi_data_fifo_v2_1_vl_rfs.v" \

vlog -work axi_register_slice_v2_1_19  -v2k5 "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/ec67/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/8c62/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ip/Zynq_Design_processing_system7_0_0" "+incdir+/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/4d88/hdl/axi_register_slice_v2_1_vl_rfs.v" \

vlog -work axi_protocol_converter_v2_1_19  -v2k5 "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/ec67/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/8c62/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ip/Zynq_Design_processing_system7_0_0" "+incdir+/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/c83a/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/ec67/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/8c62/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ip/Zynq_Design_processing_system7_0_0" "+incdir+/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/Zynq_Design/ip/Zynq_Design_auto_pc_0/sim/Zynq_Design_auto_pc_0.v" \

vcom -work lib_cdc_v1_0_2 -93 \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/ef1e/hdl/lib_cdc_v1_0_rfs.vhd" \

vcom -work proc_sys_reset_v5_0_13 -93 \
"../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/8842/hdl/proc_sys_reset_v5_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -93 \
"../../../bd/Zynq_Design/ip/Zynq_Design_rst_ps7_0_51M_0/sim/Zynq_Design_rst_ps7_0_51M_0.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/ec67/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ipshared/8c62/hdl" "+incdir+../../../../DesignSuiteProject.srcs/sources_1/bd/Zynq_Design/ip/Zynq_Design_processing_system7_0_0" "+incdir+/home/roberto/Vivado/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/Zynq_Design/sim/Zynq_Design.v" \

vlog -work xil_defaultlib \
"glbl.v"

