/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"

#include <stdlib.h> // Standard C functions, e.g. exit()
#include <stdbool.h> // Provides a Boolean data type for ANSI/ISO-C
#include "xparameters.h" // Parameter definitions for processor peripherals
#include "xscugic.h"
// Processor interrupt controller device driver
//#include "XHls_macc.h"
#include "xhls_macc.h"

typedef double basetype;

// Device driver for HLS HW block

// HLS macc HW instance
XHls_macc HlsMacc;
//Interrupt Controller Instance
XScuGic ScuGic;

volatile static int RunHlsMacc = 0;
volatile static int ResultAvailHlsMacc = 0;

int hls_macc_init(XHls_macc *hls_maccPtr)
{
	XHls_macc_Config *cfgPtr;
	int status;
	cfgPtr = XHls_macc_LookupConfig(XPAR_XHLS_MACC_0_DEVICE_ID);
	if (!cfgPtr) {
		print("ERROR: Lookup of accelerator configuration failed.\n\r");
		return XST_FAILURE;
	}
	status = XHls_macc_CfgInitialize(hls_maccPtr, cfgPtr);
	if (status != XST_SUCCESS) {
		print("ERROR: Could not initialize accelerator.\n\r");
		return XST_FAILURE;
	}
	return status;
}

void hls_macc_start(void *InstancePtr){
	XHls_macc *pAccelerator = (XHls_macc *)InstancePtr;
	XHls_macc_InterruptEnable(pAccelerator,1);
	XHls_macc_InterruptGlobalEnable(pAccelerator);
	XHls_macc_Start(pAccelerator);
}

void hls_macc_isr(void *InstancePtr){
	XHls_macc *pAccelerator = (XHls_macc *)InstancePtr;
	//Disable the global interrupt
	XHls_macc_InterruptGlobalDisable(pAccelerator);
	//Disable the local interrupt
	XHls_macc_InterruptDisable(pAccelerator,0xffffffff);
	// clear the local interrupt
	XHls_macc_InterruptClear(pAccelerator,1);
	ResultAvailHlsMacc = 1;
	// restart the core if it should run again
	if(RunHlsMacc){
		hls_macc_start(pAccelerator);
	}
}

int setup_interrupt()
{
	//This functions sets up the interrupt on the ARM
	int result;
	XScuGic_Config *pCfg =
	XScuGic_LookupConfig(XPAR_SCUGIC_SINGLE_DEVICE_ID);
	if (pCfg == NULL){
		print("Interrupt Configuration Lookup Failed\n\r");
		return XST_FAILURE;
	}
	result = XScuGic_CfgInitialize(&ScuGic,pCfg,pCfg->CpuBaseAddress);
	if(result != XST_SUCCESS){
		return result;
	}
	// self-test
	result = XScuGic_SelfTest(&ScuGic);
	if(result != XST_SUCCESS){
		return result;
	}
	// Initialize the exception handler
	Xil_ExceptionInit();
	// Register the exception handler
	//print("Register the exception handler\n\r");
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
	(Xil_ExceptionHandler)XScuGic_InterruptHandler,&ScuGic);
	//Enable the exception handler
	Xil_ExceptionEnable();
	// Connect the Adder ISR to the exception table
	//print("Connect the Adder ISR to the Exception handler table\n\r");
	result = XScuGic_Connect(&ScuGic,
	XPAR_FABRIC_HLS_MACC_0_INTERRUPT_INTR,
	(Xil_InterruptHandler)hls_macc_isr,&HlsMacc);
	if(result != XST_SUCCESS){
		return result;
	}
	//print("Enable the Adder ISR\n\r");
	XScuGic_Enable(&ScuGic,XPAR_FABRIC_HLS_MACC_0_INTERRUPT_INTR);
	return XST_SUCCESS;
}

void sw_macc(basetype x, basetype *y)
{
    basetype s = 0;
    for (int i = 1; i <= 100; i++)
        s += i * (x - (5 * i));
    *y = s;
}

int main()
{
	print("Program to test communication with HLS MACC peripheral in PL\n\r");
	basetype x, y, yref;
	int status;

	x = 20;

	//Setup the matrix mult
	status = hls_macc_init(&HlsMacc);
	if(status != XST_SUCCESS){
		print("HLS peripheral setup failed\n\r");
		exit(-1);
	}

	//Setup the interrupt
	status = setup_interrupt();
	if(status != XST_SUCCESS){
		print("Interrupt setup failed\n\r");
		exit(-1);
	}

	//set the input parameters of the HLS block
	XHls_macc_Set_x(&HlsMacc, x);
	//XHls_macc_SetAccum_clr(&HlsMacc, 1);
	if (XHls_macc_IsReady(&HlsMacc))
		print("HLS peripheral is ready. Starting... ");
	else {
		print("!!! HLS peripheral is not ready! Exiting...\n\r");
		exit(-1);
	}
	if (0) { // use interrupt
		hls_macc_start(&HlsMacc);
		while(!ResultAvailHlsMacc)
			; // spin
		y = XHls_macc_Get_y(&HlsMacc);
		print("Interrupt received from HLS HW.\n\r");
	} else { // Simple non-interrupt driven test
		XHls_macc_Start(&HlsMacc);
		do {
			y = XHls_macc_Get_y(&HlsMacc);
		} while (!XHls_macc_IsReady(&HlsMacc));
		print("Detected HLS peripheral complete. Result received.\n\r");
	}

	//call the software version of the function
	sw_macc(x, &yref);
	printf("Result from HW: %d; Result from SW: %d\n\r", (int)y, (int)yref);
	if (y == yref) {
		print("*** Results match ***\n\r");
		status = 0;
	}
	else {
		print("!!! MISMATCH !!!\n\r");
		status = -1;
	}

	cleanup_platform();
	return status;
}
